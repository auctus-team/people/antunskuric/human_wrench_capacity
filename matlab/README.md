# Matlab code

In this directory you can find three implemented algorithms:
- Iterative convex hull method: `polytope_chm`
- Ray shooting method (Carmichael et al.): `polytope_rsm`  
- Hyper plane shifting method: `hyperplane_shifting_method`
- Finding maximal under-approximation error of the found set of vertices in implemented in: `calc_error` 

In the `data` folder you can find three datasets:
- 1000 random poses of the generic 7 dof 20 muscle upper limb model: `arm720_data_1000.mat`
- 1000 random poses of the MOBL-ARMS 7 dof 50 muscle upper limb model, reduced to 32 muscles by removing ones associated to fingers: `arm732_data_1000.mat`
- 100 random poses of the MOBL-ARMS 7 dof 50 muscle upper limb model: `arm750_data_100.mat`

### Interactive demonstration
`demo.mlx` is the matlab live script with benchmarking and comparison of the three algorithms.