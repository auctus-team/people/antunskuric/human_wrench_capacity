function [max_error, vol, f_max] = calc_error(f_p, J, N, F_min, F_max)
    
[m, n] = size(J); 
% svd of jacobian
[u s v] = svd(J);
V1 = v(:, 1:m);
V2 = v(:, m+1:end);
M = pinv(J')*N;
Aeq = V2'*N;
beq = zeros(n-m,1);
opt = optimset('linprog');
opt.Display = 'off';

% opt.OptimalityTolerance = '1e-10';
opt.Algorithm = 'interior-point';
% caclualte the convex hull
[faces_p, vol] = convhulln(f_p',[{'QJ', 'Pp'}]);

res1 = linprog(-(ones(1,m)*M)',  [], [], Aeq, beq, F_min',F_max', opt);
res2 = linprog((ones(1,m)*M)',  [], [], Aeq, beq, F_min',F_max', opt);
f_max = max(norm(M*res1),norm(M*res2));

f_center = mean(f_p')';
dist = 0;
for i = 1:length(faces_p(:,1))
    face = faces_p(i,:);

    if min([norm(f_p(:,face(1))-f_p(:,face(2))), norm(f_p(:,face(1))-f_p(:,face(3))), norm(f_p(:,face(2))-f_p(:,face(3)))]) < 1e-5 
       continue 
    end
    
    % calculate the normal vector to the face
    normal_k =  cross(f_p(:,face(1)) -f_p(:,face(2)),f_p(:,face(1)) -f_p(:,face(3)));
    normal_k = normal_k/norm(normal_k);
    vec = normal_k'; 

    % taker any (first one) vertex which is not in the face
    dir = vec*(f_p(:,face) - f_center) > 0;

    % use linear programming to find a vertex in the vec direciton
    if dir
        res = linprog(-(vec*M)',  [], [], Aeq, beq, F_min',F_max', opt);
    else
        res = linprog((vec*M)',  [], [], Aeq, beq, F_min',F_max',opt);
    end

    % vertex distance from the face
    distance = abs( vec * ( M*res - f_p(:,face(1)) ) );
    if distance > dist
        dist = distance;
    end
end 
max_error = dist;
end

