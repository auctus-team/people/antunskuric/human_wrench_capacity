function [f_vert, F_vert, H, d, cnt] = polytope_chm(J, N, F_min, F_max, precision)
%   Calculates the wrench feasabiliy polytope of the musuloskeletal model with
%   the specified jacobian matrix J, the lever arm matrix N and the muscle
%   force limits. The user can additionally imput the level of precision for
%   the algorihm to achieve.
%   This function implements the iterative Convex-hull method
%
%   Inputs:
%       J           :   End-effector jacobian matrix
%       N           :   Moment arm matrix 
%       F_min       :   Minimal muscle-tendon forces
%       F_max       :   Maxilam muslce tendon forces
%       precision   :   precision bound to achieve
%
%   Outputs:
%       f_vert      :   Vertices of the polytope
%       F_vert      :   Muscular forces generating the vertices f_vert
%       H           :   matrix H of the half space rep
%       d           :   vector d of the half space rep
%       cnt         :   number of linear problems solved
%

[m, n] = size(J); 
if nargin <= 4
     precision = 1e-1;
end

% max lp count
max_count = 200000;

% svd of jacobian
[u s v] = svd(J);
V1 = v(:, 1:m);
V2 = v(:, m+1:end);
M = pinv(J')*N;

if m==n && all(J == eye(m),'all')
   [u,~] = svd(N);
end

% optimisation setup
Aeq = V2'*N;
beq = zeros(n-m,1);
opt = optimset('linprog');
opt.Display = 'off';
% opt.Optimalityprecisionerance = '1e-10';
opt.Algorithm = 'interior-point';
n_fac = 0;

convhull_opt = [{'Qt'},{'Qx'},{'Pp'}];



F_vert = [];
for i = 1:m
    vec= u(:,i)';
    res = linprog(-(vec*M)',  [], [], Aeq, beq, F_min',F_max',opt);
    F_vert = [F_vert, res];
    res = linprog((vec*M)',  [], [], Aeq, beq, F_min',F_max',opt);
    F_vert = [F_vert, res];
end

% calculate the forces 
if ~isempty(F_vert)
    f_p  = M*F_vert;
else
    f_p = [];
end
    
% calculate the initial convex hull
enough = 0;
while ~enough
    
    % do a convex hull an make sure that the points are good enough
    try
        % caclualte the convex hull
        faces_p = convhulln(f_p',convhull_opt);
        enough = 1;
    catch
        try
            % if the 'Qt'(default) parameter trows exception try with 'QJ'
            convhull_opt(1) = {'QJ'};
            faces_p = convhulln(f_p',convhull_opt);
            warning('Using QJ option');
            enough = 1;
        catch
            % if the 'Qt' and 'QJ' donty work, we need mode points
            convhull_opt(1) = {'Qt'};
            enough = 0;
            
            warning('Initial convex-hull not enough: additional radnom vecotrs used');
            % Do n_dir random linprogs to find aditional set of points
            for i = 1:10
                vec = rand(1,m)*2-1;
                vec= vec/norm(vec);
                res = linprog(-(vec*M)',  [], [], Aeq, beq, F_min',F_max',opt);
                F_vert = [F_vert, res];
            end
            % calculate the forces 
            f_p  = M*F_vert;
        end
    end
end

% initialize the half space rep
H =[];
d =[];

cnt = 2*m;

F_final = containers.Map;

d_max = inf;
while ( d_max > precision ) && cnt < max_count
    
    f_center = mean(f_p')';
    
    F_vert_new = [];
     
    d_max = 0;
    for i = 1:length(faces_p(:,1))
        face = faces_p(i,:);
        % create a string index of the face 
        % this value is used as a hash map index
        % to store dyamically the faces that have been found as final
        map_ind = mat2str(sort(face));
        % check if this face (face index) has been found as final
        if(F_final.isKey(map_ind))
           continue; 
        end
        
        % if for some reason two vertices of the face too close (same)
        if m == 2 && norm(f_p(:,face(1))-f_p(:,face(2))) < 1e-5 % if 2D
            F_final(map_ind) = 1; 
            continue;
        elseif m == 3 && min([norm(f_p(:,face(1))-f_p(:,face(2))), norm(f_p(:,face(1))-f_p(:,face(3))), norm(f_p(:,face(2))-f_p(:,face(3)))]) < 1e-5  % if 3D
            F_final(map_ind) = 1; 
            continue;
        end
        
        % calculate the normal vector to the face
        if m == 3 % if 3D
            normal_k =  cross(f_p(:,face(1)) -f_p(:,face(2)),f_p(:,face(1)) -f_p(:,face(3)));
            normal_k = normal_k/norm(normal_k);
            vec = normal_k'; 
        else % if 2D or 4D+  
            ikm = f_p(:,face(1)) - f_p(:,face(2:end)); 
            vec = null(ikm')';
            vec = vec(end,:);
        end
        
        if ~isempty(H) && min(vecnorm((H + vec)')) < 1e-5
            continue
        end
                
        if any(isnan(face)) || any(isnan(vec))
            F_final(map_ind) = 1;
            warning("Nan in face index - skipping");
           continue;
        end
        
        % taker any (first one) vertex which is not in the face
        dir = vec*(f_p(:,face) - f_center) > 0;
        
        % use linear programming to find a vertex in the vec direciton
        if dir
            res = linprog(-(vec*M)',  [], [], Aeq, beq, F_min',F_max',F_vert(:,face(1)), opt);
        else
            res = linprog((vec*M)',  [], [], Aeq, beq, F_min',F_max',F_vert(:,face(1)),opt);
        end

        % a simple counter of linprog execuitions
        cnt = cnt+1;
        
        % vertex distance from the face
        distance = abs( vec * ( M*res - f_p(:,face(1)) ) );
        if distance > d_max
            d_max = distance;
        end
        if distance > precision
            % new vertex found
            F_vert_new = [F_vert_new, res]; 
        else
            F_final(map_ind) = 1;
            % this face is final 
            d_i = (vec*f_p(:,face(1)));
            H = [H; -vec];
            d = [d; -d_i];
        end
    end
    if ~isempty(F_vert_new)
        F_vert = [F_vert, F_vert_new];
        f_p  = M*F_vert;

        % calculating the convex hull
        try
            % caclualte the convex hull with default settings
            faces_p = convhulln(f_p',convhull_opt);
        catch
            try
                % if the 'Qt'(default) parameter trows exception try 'QJ'
                convhull_opt(1) = {'QJ'};
                faces_p = convhulln(f_p',convhull_opt);
                warning('Using QJ option half-way');
            catch
                % if the 'Qt' and 'QJ' dont work, we have an error 
                warning('Convex hull problem half-way through: stopping');
                break;
            end
        end
    else
       break 
    end
end
% cnt
f_vert  = M*F_vert;

end

