function [f_vert, F_vert, H, d, cnt] = polytope_carmichael(J, N, F_min, F_max, n_dir)
%   Ray shooting agorihtm introduced by Carmichael et al. 
%   Towards using musculoskeletal models for intelligent control of physically assistive robots | IEEE Conference Publication
%
%   Inputs:
%       J           :   End-effector jacobian matrix
%       N           :   Moment arm matrix 
%       F_min       :   Minimal muscle-tendon forces
%       F_max       :   Maxilam muslce tendon forces
%       n_dir       :   number of ray directions per euler angle
%
%   Outputs:
%       f_vert      :   Vertices of the polytope
%       F_vert      :   Muscular forces generating the vertices f_vert
%       H           :   matrix H of the half space rep - not defined
%       d           :   vector d of the half space rep - not defined
%       cnt         :   number of linear problems solved
%

R = @(t) [cos(t) -sin(t); sin(t) cos(t)];

% svd of jacobian
[m, ~] = size(J);
[n, L] = size(N);

% optimisation setup
opt = optimset('linprog');
opt.Display = 'off';
% opt.OptimalityTolerance = '1e-10';
opt.Algorithm = 'interior-point';

cnt =0;

% sphere sampling
% http://extremelearning.com.au/how-to-generate-uniformly-random-points-on-n-spheres-and-n-balls/
if m == 3
    u_map = [];
    % prepare the  map of directions where to go
    for  theta_i = 1:n_dir 
        theta = 2 * pi * theta_i / n_dir ; 
        for  phi_i = 1:n_dir 
            phi = acos(2*phi_i/n_dir - 1);
            x   = sin(theta)* cos(phi);
            y   = sin(theta)*sin(phi);
            z   = cos (theta);
            u_map = [u_map, [x;y;z]];
        end
    end
elseif m == 2
    u_map = [];
    u = zeros(m,1);
    u(1) = 1;
    % prepare the  map of directions where to go
    for  x = 1:n_dir 
        u_map = [u_map, R( d_angle * x - pi/2)*u];
    end
end

A =  [ eye(L) ; -eye(L)];
b =  [ F_max'; -F_min'];
% find all the intersection vertices
F_vert = [];
for  k = 1:length(u_map(1,:))
    u = u_map(:,k);

    r = J'*u;
    [~, i] = max(abs(r));
    i = i(1);
    r_i = r(i);

    Aeq = -r./r_i*N(i,:) + N;
    beq = zeros(1,n);%-N*F_min' + r./r_i*N(i,:)*F_min';
    Aeq(i,:) = [];
    beq(i) = [];
    
    c = N(i,:)'/r_i;

    res = linprog(-c,  A, b, Aeq, beq, [], [], F_min, opt);
    F_vert = [F_vert, res];
 
    cnt = cnt+1;
end
% cnt
f_vert = pinv(J')*N*F_vert;

H= [];
d =[];

end


